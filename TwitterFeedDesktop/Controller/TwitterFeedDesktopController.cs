﻿using System.Collections.Generic;
using TwitterFeedDesktop.Model;

namespace TwitterFeedDesktop.Controller
{
    /// <summary>
    ///     Controller class between model and view.
    /// </summary>
    public class TwitterFeedDesktopController
    {
        /// <summary>
        ///     The searcher
        /// </summary>
        private readonly SearchTwitter searcher;

        public TwitterFeedDesktopController()
        {
            this.searcher = new SearchTwitter();
        }

        /// <summary>
        ///     Gets the tweets.
        /// </summary>
        /// <param name="searchHashTag">The search hash tag.</param>
        /// <returns></returns>
        public List<string> GetTweets(HashTags searchHashTag)
        {
            return this.searcher.PerformSearch(searchHashTag);
        }

        public List<string> GetAllTweets()
        {
            return this.searcher.AllTweets;
        }

        /// <summary>
        ///     Gets the category tweets.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public List<Tweet> GetCategoryTweets(HashTags id)
        {
            switch (id)
            {
                case HashTags.Sports:
                    return this.searcher.SportsTweets();
                case HashTags.Entertainment: //entertainment
                    return this.searcher.EntertainmentTweets();
                case HashTags.News: // news
                    return this.searcher.NewsTweets();
                case HashTags.Breaking: //breaking
                    return this.searcher.BreakingTweets();
                default:
                    return null;
            }
        }
    }
}