﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json.Linq;

namespace TwitterFeedDesktop.Model
{
    /// <summary>
    ///     Searching class. Searches through twitter using appropriate hashtags.
    /// </summary>
    public class SearchTwitter
    {
        /// <summary>
        ///     Gets all tweets.
        /// </summary>
        /// <value>
        ///     All tweets.
        /// </value>
        public List<string> AllTweets { get; private set; }

        private List<string> breakingResults;

        private readonly List<Tweet> breakingTweets;
        private List<string> entertainmentResults;
        private readonly List<Tweet> entertainmentTweets;
        private List<string> newsResults;
        private readonly List<Tweet> newsTweets;
        private string objectText;
        private List<string> sportsResults;
        private readonly List<Tweet> sportTweets;
        private List<string> tweetText;

        public SearchTwitter()
        {
            this.sportsResults = new List<string>();
            this.sportTweets = new List<Tweet>();

            this.newsResults = new List<string>();
            this.newsTweets = new List<Tweet>();

            this.entertainmentResults = new List<string>();
            this.entertainmentTweets = new List<Tweet>();

            this.breakingResults = new List<string>();
            this.breakingTweets = new List<Tweet>();
        }

        public List<Tweet> SportsTweets()
        {
            return this.sportTweets;
        }

        public List<Tweet> NewsTweets()
        {
            return this.newsTweets;
        }

        public List<Tweet> EntertainmentTweets()
        {
            return this.entertainmentTweets;
        }

        public List<Tweet> BreakingTweets()
        {
            return this.breakingTweets;
        }

        /// <summary>
        ///     Performs the search.
        /// </summary>
        /// <param name="hashTag">The hash tag.</param>
        /// <returns></returns>
        public List<string> PerformSearch(HashTags hashTag)
        {
            var searchResults = new List<string>();

            switch (hashTag)
            {
                case HashTags.Sports:
                    this.sportsResults = this.performSportsSearch();

                    this.makeTweets(this.sportsResults, this.sportTweets, 0);

                    searchResults = this.sportsResults;
                    break;

                case HashTags.Entertainment:
                    this.entertainmentResults = this.performEntertainmentSearch();

                    this.makeTweets(this.entertainmentResults, this.entertainmentTweets, 1);

                    searchResults = this.entertainmentResults;
                    break;

                case HashTags.News:
                    this.newsResults = this.performNewsSearch();

                    this.makeTweets(this.newsResults, this.newsTweets, 2);

                    searchResults = this.newsResults;
                    break;

                case HashTags.Breaking:
                    this.breakingResults = this.performBreakingSearch();

                    this.makeTweets(this.breakingResults, this.breakingTweets, 3);

                    searchResults = this.breakingResults;
                    break;
            }

            this.AllTweets = this.sportsResults.Concat(this.entertainmentResults)
                                 .Concat(this.newsResults)
                                 .Concat(this.breakingResults)
                                 .ToList();

            return searchResults;
        }

        /// <summary>
        /// Makes the tweets from string collection.
        /// </summary>
        /// <param name="list">The list.</param>
        /// <param name="tweets">The tweets.</param>
        /// <param name="id">The identifier.</param>
        private void makeTweets(IEnumerable<string> list, ICollection<Tweet> tweets, int id)
        {

            var breakFreeTweets = new List<string>();

            foreach (var currentTweet in list)
            {
                breakFreeTweets.Add(currentTweet.Replace(Environment.NewLine, ""));
            }

            foreach (var breakFreeTweet in breakFreeTweets)
            {
                Console.Write(breakFreeTweet);
            }

            foreach (var result in breakFreeTweets)
            {
                var currTweet = new Tweet
                {
                    Id = id,
                    TweetText = result
                };

                tweets.Add(currTweet);
            }
        }

        /// <summary>
        ///     Performs the breaking search.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        private List<string> performBreakingSearch()
        {
            return this.performHashtagSearch("#breaking");
        }

        /// <summary>
        ///     Performs the news search.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        private List<string> performNewsSearch()
        {
            return this.performHashtagSearch("#news");
        }

        /// <summary>
        ///     Performs the entertainment search.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        private List<string> performEntertainmentSearch()
        {
            return this.performHashtagSearch("#entertainment");
        }

        /// <summary>
        ///     Performs the sports search.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        private List<string> performSportsSearch()
        {
            return this.performHashtagSearch("#sports");
        }

        /// <summary>
        ///     Performs the hashtag search.
        /// </summary>
        /// <param name="hashTagQuery">The hash tag query.</param>
        /// <returns></returns>
        private List<string> performHashtagSearch(string hashTagQuery)
        {
            this.tweetText = new List<string>();

            var response = performRequest(hashTagQuery);

            if (response.GetResponseStream() == Stream.Null)
            {
                return this.tweetText;
            }

            var reader = new StreamReader(response.GetResponseStream());

            this.objectText = reader.ReadToEnd();

            var jSonResponse = JObject.Parse(this.objectText);

            var status = jSonResponse.SelectToken("statuses");

            for (var i = 0; i < 14; i++)
            {
                if (status[i].SelectToken("lang").ToString() == "en")
                {
                    this.tweetText.Add(status[i].SelectToken("text").ToString());
                }
            }

            if (this.tweetText.Count > 5)
            {
                this.tweetText = this.tweetText.GetRange(0, 5);
            }

            return this.tweetText;
        }

        /// <summary>
        ///     Performs the request.
        /// </summary>
        /// <param name="hashTagQuery">The hash tag query.</param>
        /// <returns></returns>
        private static HttpWebResponse performRequest(string hashTagQuery)
        {
            var resourceUrl = "https://api.twitter.com/1.1/search/tweets.json";

            // oauth application keys

            var authHeader = builtRequestHeader(hashTagQuery, resourceUrl);

            ServicePointManager.Expect100Continue = false;

            var postBody = "q=" + Uri.EscapeDataString(hashTagQuery); //
            resourceUrl += "?" + postBody;

            var request = (HttpWebRequest) WebRequest.Create(resourceUrl);
            request.Headers.Add("Authorization", authHeader);
            request.Method = "GET";
            request.ContentType = "application/x-www-form-urlencoded";
            var response = (HttpWebResponse) request.GetResponse();
            return response;
        }

        /// <summary>
        ///     Builts the request header.
        /// </summary>
        /// <param name="hashTagQuery">The hash tag query.</param>
        /// <param name="resourceUrl">The resource URL.</param>
        /// <returns></returns>
        private static string builtRequestHeader(string hashTagQuery, string resourceUrl)
        {
            var oauthNonce = Convert.ToBase64String(new ASCIIEncoding().GetBytes(DateTime.Now.Ticks.ToString()));
            var timeSpan = DateTime.UtcNow
                           - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            var oauthTimestamp = Convert.ToInt64(timeSpan.TotalSeconds).ToString();

            var baseString = string.Format(BaseFormat,
                OauthConsumerKey,
                oauthNonce,
                OauthSignatureMethod,
                oauthTimestamp,
                OauthToken,
                OauthVersion,
                Uri.EscapeDataString(hashTagQuery)
                );

            baseString = string.Concat("GET&", Uri.EscapeDataString(resourceUrl), "&", Uri.EscapeDataString(baseString));

            var compositeKey = string.Concat(Uri.EscapeDataString(OauthConsumerSecret),
                "&", Uri.EscapeDataString(OauthTokenSecret));

            string oauthSignature;
            using (var hasher = new HMACSHA1(Encoding.ASCII.GetBytes(compositeKey)))
            {
                oauthSignature = Convert.ToBase64String(
                    hasher.ComputeHash(Encoding.ASCII.GetBytes(baseString)));
            }

            var authHeader = string.Format(HeaderFormat,
                Uri.EscapeDataString(oauthNonce),
                Uri.EscapeDataString(OauthSignatureMethod),
                Uri.EscapeDataString(oauthTimestamp),
                Uri.EscapeDataString(OauthConsumerKey),
                Uri.EscapeDataString(OauthToken),
                Uri.EscapeDataString(oauthSignature),
                Uri.EscapeDataString(OauthVersion)
                );
            return authHeader;
        }

        #region Constants

        /// <summary>
        ///     The oauth token
        /// </summary>
        private const string OauthToken = "4145927717-TGuSVxJdaMk3ZmufwFs4q7LkWmbAsLAyjriWFhl";

        /// <summary>
        ///     The oauth token secret
        /// </summary>
        private const string OauthTokenSecret = "GFp5CTjknuag4MOF98C86Xi5z3UkM7kkNaUDrL2Mpu9L0";

        /// <summary>
        ///     The oauth consumer key
        /// </summary>
        private const string OauthConsumerKey = "qMs8DKgN22efsvdH6ilRw1EeY";

        /// <summary>
        ///     The oauth consumer secret
        /// </summary>
        private const string OauthConsumerSecret = "aRdpcnSdTMczCnkrbo38qRKy9DZH89v2UXLViV2dj2TlFx8nmU";

        /// <summary>
        ///     The oauth version
        /// </summary>
        private const string OauthVersion = "1.0";

        /// <summary>
        ///     The oauth signature method
        /// </summary>
        private const string OauthSignatureMethod = "HMAC-SHA1";

        /// <summary>
        ///     The base format
        /// </summary>
        private const string BaseFormat = "oauth_consumer_key={0}&oauth_nonce={1}&oauth_signature_method={2}" +
                                          "&oauth_timestamp={3}&oauth_token={4}&oauth_version={5}&q={6}";

        /// <summary>
        ///     The header format
        /// </summary>
        private const string HeaderFormat = "OAuth oauth_nonce=\"{0}\", oauth_signature_method=\"{1}\", " +
                                            "oauth_timestamp=\"{2}\", oauth_consumer_key=\"{3}\", " +
                                            "oauth_token=\"{4}\", oauth_signature=\"{5}\", " +
                                            "oauth_version=\"{6}\"";

        #endregion
    }
}