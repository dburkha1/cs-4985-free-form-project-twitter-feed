﻿namespace TwitterFeedDesktop.Model
{
    /// <summary>
    ///     Hash tag enum to store different types of hashtag.
    /// </summary>
    public enum HashTags
    {
        /// <summary>
        ///     The breaking
        /// </summary>
        Breaking,

        /// <summary>
        ///     The news
        /// </summary>
        News,

        /// <summary>
        ///     The entertainment
        /// </summary>
        Entertainment,

        /// <summary>
        ///     The sports
        /// </summary>
        Sports
    }

    /// <summary>
    ///     Tweet model object
    /// </summary>
    public class Tweet
    {
        /// <summary>
        ///     Gets or sets the identifier.
        /// </summary>
        /// <value>
        ///     The identifier.
        /// </value>
        public int Id { get; set; }

        /// <summary>
        ///     Gets or sets the name.
        /// </summary>
        /// <value>
        ///     The name.
        /// </value>
        public string TweetText { get; set; }
    }
}