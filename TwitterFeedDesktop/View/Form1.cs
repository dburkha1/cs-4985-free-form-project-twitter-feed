﻿using System;
using System.IO.Ports;
using System.Windows.Forms;
using TwitterFeedDesktop.Controller;
using TwitterFeedDesktop.Model;
using TwitterFeedDesktop.Properties;

namespace TwitterFeedDesktop.View
{
    /// <summary>
    ///     Desktop form for TweetDesk
    /// </summary>
    public partial class TwitterFeedDesktopForm : Form
    {
        /// <summary>
        ///     The controller
        /// </summary>
        private readonly TwitterFeedDesktopController controller;

        /// <summary>
        ///     Initializes a new instance of the <see cref="TwitterFeedDesktopForm" /> class.
        /// </summary>
        public TwitterFeedDesktopForm()
        {
            this.InitializeComponent();
            this.controller = new TwitterFeedDesktopController();

        }

        /// <summary>
        ///     Handles the 1 event of the TwitterFeedDesktopForm_Load control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void TwitterFeedDesktopForm_Load_1(object sender, EventArgs e)
        {
          

            this.refreshTimer.Start();
            this.displayTweetsFromTwitter();
        }

        /// <summary>
        ///     Displays the tweets from twitter.
        /// </summary>
        private void displayTweetsFromTwitter()
        {
            this.tweetTextBox.Text = string.Empty;

            this.refreshTweets();

            var currentTweets = this.controller.GetAllTweets();

            foreach (var tweet in currentTweets)
            {
                this.tweetTextBox.Text += tweet + "\r\n\r\n";
            }
            try
            {
                this.arduinoSerialPort.Open();
                this.writeTweets(HashTags.Sports);

                this.writeTweets(HashTags.Entertainment);
                this.writeTweets(HashTags.News);
                this.writeTweets(HashTags.Breaking);
                this.arduinoSerialPort.Close();
                
            }
            catch
            {
                MessageBox.Show(Resources.SerialPortErrorMessage);
            }
            
        }

        /// <summary>
        ///     Refreshes the tweets.
        /// </summary>
        private void refreshTweets()
        {
            this.controller.GetTweets(HashTags.News);
            this.controller.GetTweets(HashTags.Sports);
            this.controller.GetTweets(HashTags.Entertainment);
            this.controller.GetTweets(HashTags.Breaking);
        }

        /// <summary>
        ///     Handles the Tick event of the refreshTimer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void refreshTimer_Tick(object sender, EventArgs e)
        {
            this.displayTweetsFromTwitter();
        }

        /// <summary>
        ///     Writes the tweets.
        /// </summary>
        /// <param name="hashtag">The hashtag.</param>
        private void writeTweets(HashTags hashtag)
        {
            var categoryTweets = this.controller.GetCategoryTweets(hashtag);
            this.arduinoSerialPort.WriteLine(categoryTweets[0].TweetText.PadRight(140, ' '));
            this.arduinoSerialPort.WriteLine(categoryTweets[1].TweetText.PadRight(140, ' '));
            this.arduinoSerialPort.WriteLine(categoryTweets[2].TweetText.PadRight(140, ' '));

        }

        private void arduinoSerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            var sp = (SerialPort) sender;
            var currentCategory = sp.ReadLine();
            Console.WriteLine(currentCategory);

            switch (currentCategory)
            {
                case "Sports":
                    this.writeTweets(HashTags.Sports);
                    break;
                case "Entertainment":
                    this.writeTweets(HashTags.Entertainment);
                    break;
                case "News":
                    this.writeTweets(HashTags.News);
                    break;
                case "Breaking":
                    this.writeTweets(HashTags.Breaking);
                    break;
            }
        }
    }
}