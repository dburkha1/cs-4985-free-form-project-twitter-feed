﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.IO.Ports;
using System.Windows.Forms;
using TwitterFeedDesktop.Model;

namespace TwitterFeedDesktop.View
{
    partial class TwitterFeedDesktopForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tweetTextBox = new System.Windows.Forms.TextBox();
            this.arduinoSerialPort = new System.IO.Ports.SerialPort(this.components);
            this.refreshTimer = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // tweetTextBox
            // 
            this.tweetTextBox.Location = new System.Drawing.Point(12, 12);
            this.tweetTextBox.Multiline = true;
            this.tweetTextBox.Name = "tweetTextBox";
            this.tweetTextBox.ReadOnly = true;
            this.tweetTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tweetTextBox.Size = new System.Drawing.Size(712, 698);
            this.tweetTextBox.TabIndex = 0;
            // 
            // arduinoSerialPort
            // 
            this.arduinoSerialPort.PortName = "COM4";
            this.arduinoSerialPort.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.arduinoSerialPort_DataReceived);
            // 
            // refreshTimer
            // 
            this.refreshTimer.Interval = 300000;
            this.refreshTimer.Tick += new System.EventHandler(this.refreshTimer_Tick);
            // 
            // TwitterFeedDesktopForm
            // 
            this.ClientSize = new System.Drawing.Size(736, 722);
            this.Controls.Add(this.tweetTextBox);
            this.Name = "TwitterFeedDesktopForm";
            this.Text = "TwitterDesktopFeed";
            this.Load += new System.EventHandler(this.TwitterFeedDesktopForm_Load_1);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion


        private TextBox tweetTextBox;
        private SerialPort arduinoSerialPort;
        private Timer refreshTimer;
    }
}

