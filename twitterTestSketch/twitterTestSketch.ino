#include <Esplora.h>
#include <String.h>
#include <TFT.h>
#include <SPI.h>

String sports1;
String sports2;
String sports3;

char entertainment0[150];
char entertainment1[150];
char entertainment2[150];


char news0[150];
char news1[150];
char news2[150];


char breaking0[150];
char breaking1[150];
char breaking2[150];


void setup() {
  Serial.begin(9600);
  EsploraTFT.begin();
  EsploraTFT.setTextWrap(true);
  EsploraTFT.background(255, 255, 255);
  showStartScreen();
}

void loop() {

  if (Serial.available()) {

    readSportsTweets();
    readEntertainmentTweets();
    readNewsTweets();
    readBreakingTweets();
  }

  if (Esplora.readButton(SWITCH_UP) == LOW) {

    EsploraTFT.background(255, 255, 255);
    writeSportsTweets();

  } else if (Esplora.readButton(SWITCH_LEFT) == LOW) {

    EsploraTFT.background(255, 255, 255);
    writeNewsTweets();

  } else if (Esplora.readButton(SWITCH_RIGHT) == LOW) {

    EsploraTFT.background(255, 255, 255);
    writeEntertainmentTweets();

  } else if (Esplora.readButton(SWITCH_DOWN) == LOW) {

    EsploraTFT.background(255, 255, 255);
    writeBreakingTweets();

  }

}

void showStartScreen() {
  EsploraTFT.background(255, 255, 255);
  EsploraTFT.stroke(0, 0, 0);
  EsploraTFT.setTextSize(2);
  EsploraTFT.text("TweetDesk", 10, 0);
  EsploraTFT.setTextSize(1);
  EsploraTFT.text("Press up for Sports", 10, 30);
  EsploraTFT.text("Press right for", 10, 50);
  EsploraTFT.text("Entertainment", 10, 60);
  EsploraTFT.text("Press left for News", 10, 80);
  EsploraTFT.text("Press down for Breaking", 10, 100);
  
}

void readSportsTweets() {


char sport0[150];
char sport1[150];
char sport2[150];


  Serial.readBytes(sport0, 140);
  Serial.readBytes(sport1, 140);
  Serial.readBytes(sport2, 140);

sports1 = String(sport0);
sports2 = String(sport1);
sports3 = String(sport2);

}

void writeSportsTweets() {

  EsploraTFT.background(255, 255, 255);
  EsploraTFT.stroke(0, 0, 0);

  EsploraTFT.setTextSize(1);

  while (Esplora.readButton(SWITCH_UP) == HIGH && Esplora.readButton(SWITCH_DOWN) == HIGH) {


    EsploraTFT.text("Sports", 10, 0);

    //string .substring. tochararray
    //printout char array

    char charArray1[30];
    char charArray2[30];
    char charArray3[30];
    char charArray4[30];
    char charArray5[30];

    sports1.substring(0, 29).toCharArray(charArray1, 30);
    sports1.substring(30, 59).toCharArray(charArray2, 30);
    sports1.substring(60, 89).toCharArray(charArray3, 30);
    sports1.substring(90, 119).toCharArray(charArray4, 30);
    sports1.substring(120, 150).toCharArray(charArray5, 30);

    EsploraTFT.text(charArray1, 10, 20);
    EsploraTFT.text(charArray2, 10, 30);
    EsploraTFT.text(charArray3, 10, 40);
    EsploraTFT.text(charArray4, 10, 50);
    EsploraTFT.text(charArray5, 10, 60);

     /*
    EsploraTFT.text(sport0, 10, 20);
   
    EsploraTFT.text(sport1, 10, 60);

    EsploraTFT.text(sport2, 10, 90);
    */
    
    
    
    EsploraTFT.text("Press up and down to return", 10, 120);

  }

  showStartScreen();

}

void readEntertainmentTweets() {

  Serial.readBytes(entertainment0, 140);
  Serial.readBytes(entertainment1, 140);
  Serial.readBytes(entertainment2, 140);

}

void writeEntertainmentTweets() {

  EsploraTFT.background(255, 255, 255);
  EsploraTFT.stroke(0, 0, 0);
  EsploraTFT.setTextSize(1);

  while (Esplora.readButton(SWITCH_UP) == HIGH && Esplora.readButton(SWITCH_DOWN) == HIGH) {
    EsploraTFT.setTextWrap(true);
    EsploraTFT.text("Etnertainment", 0, 0);

    EsploraTFT.text(entertainment0, 0, 20);

    EsploraTFT.text(entertainment1, 0, 40);

    EsploraTFT.text(entertainment2, 0, 60);


    EsploraTFT.text("Press up and down to return", 0, 120);
  }
  showStartScreen();

}

void readNewsTweets() {

  Serial.readBytes(news0, 140);
  Serial.readBytes(news1, 140);
  Serial.readBytes(news2, 140);


}

void writeNewsTweets() {

  EsploraTFT.background(255, 255, 255);
  EsploraTFT.stroke(0, 0, 0);
  EsploraTFT.setTextSize(1);

  while (Esplora.readButton(SWITCH_UP) == HIGH && Esplora.readButton(SWITCH_DOWN) == HIGH) {
    EsploraTFT.setTextWrap(true);
    EsploraTFT.text("News", 0, 0);

    EsploraTFT.text(news0, 0, 20);

    EsploraTFT.text(news1, 0, 40);

    //newsTweets[2].toCharArray(newsCharArray, newsTweets[2].length());
    EsploraTFT.text(news2, 0, 60);


    EsploraTFT.text("Press up and down to return", 0, 120);
  }
  showStartScreen();

}

void readBreakingTweets() {

  Serial.readBytes(breaking0, 140);
  Serial.readBytes(breaking1, 140);
  Serial.readBytes(breaking2, 140);


}

void writeBreakingTweets() {

  EsploraTFT.background(255, 255, 255);
  EsploraTFT.stroke(0, 0, 0);
  EsploraTFT.setTextSize(1);

  while (Esplora.readButton(SWITCH_UP) == HIGH && Esplora.readButton(SWITCH_DOWN) == HIGH) {
    EsploraTFT.setTextWrap(true);
    EsploraTFT.text("Breaking", 0, 0);

    EsploraTFT.text(breaking0, 0, 20);

    EsploraTFT.text(breaking1, 0, 40);

    EsploraTFT.text(breaking2, 0, 60);

    EsploraTFT.text("Press up and down to return", 0, 120);
  }

  showStartScreen();

}

